﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using EasyCRMApi.Data;
using EasyCRMApi.ModelProxy;

namespace EasyCRMApi.Hubs
{
    public class ClientHub : AbstractEntityHub<ClientProxy>
    {
        public override void CreateEntity(ClientProxy entity)
        {
            try
            {
                var client = new Client
                {
                    FullName = entity.FullName
                };
                var d = new EasyCRMDataContainer();
                var clientSet = d.ClientSet;
                clientSet.Add(client);
                d.SaveChanges();
                Clients.All.clientsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void DeleteEntity(ClientProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var clientSet = d.ClientSet;
                var client = new Client { Id = entity.Id };
                clientSet.Attach(client);
                d.Entry(client).State = System.Data.Entity.EntityState.Deleted;
                d.SaveChanges();
                Clients.All.clientsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void GetAll()
        {
            Clients.Client(Context.ConnectionId).clientsUpdated(toProxyList());
        }

        public override void UpdateEntity(ClientProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var clientSet = d.ClientSet;
                var client = new Client { Id = entity.Id, FullName = entity.FullName };
                clientSet.Attach(client);
                d.Entry(client).State = System.Data.Entity.EntityState.Modified;
                d.SaveChanges();
                Clients.All.clientsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
        private List<ClientProxy> toProxyList()
        {
            var proxyList = new List<ClientProxy>();
            try
            {
                var d = new EasyCRMDataContainer();
                var clientSet = d.ClientSet;
                clientSet.ToList<Client>().ForEach(c => proxyList.Add(new ClientProxy { Id = c.Id, FullName = c.FullName }));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return proxyList;
        }
    }
}