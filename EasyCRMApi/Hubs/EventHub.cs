﻿using EasyCRMApi.Data;
using EasyCRMApi.ModelProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyCRMApi.Hubs
{
    public class EventHub : AbstractEntityHub<EventProxy>
    {
        public override void CreateEntity(EventProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var eventSet = d.EventSet;
                var eventS = new Event { CourseId = entity.CourseId, ClientId = entity.ClientId };
                eventSet.Add(eventS);
                d.SaveChanges();
                Clients.All.eventsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void DeleteEntity(EventProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var eventSet = d.EventSet;
                var eventS = new Event { Id = entity.Id };
                eventSet.Attach(eventS);
                d.Entry(eventS).State = System.Data.Entity.EntityState.Deleted;
                d.SaveChanges();
                Clients.All.eventsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void GetAll()
        {
            Clients.Client(Context.ConnectionId).eventsUpdated(toProxyList());
        }

        public override void UpdateEntity(EventProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var eventSet = d.EventSet;
                var eventS = new Event { Id = entity.Id, ClientId = entity.ClientId, CourseId = entity.CourseId };
                eventSet.Attach(eventS);
                d.Entry(eventS).State = System.Data.Entity.EntityState.Modified;
                d.SaveChanges();
                Clients.All.eventsUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
        private List<EventProxy> toProxyList()
        {
            var proxyList = new List<EventProxy>();
            try
            {
                var d = new EasyCRMDataContainer();
                var eventSet = d.EventSet;
                eventSet.ToList<Event>().ForEach(c => proxyList.Add(new EventProxy { Id = c.Id, CourseId = c.CourseId, ClientId = c.ClientId }));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return proxyList;
        }
    }
}