﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyCRMApi.Hubs
{
    public abstract class AbstractEntityHub<T> : Hub
    {
        protected log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public abstract void CreateEntity(T enity);
        public abstract void UpdateEntity(T entity);
        public abstract void DeleteEntity(T entity);
        public abstract void GetAll();
    }
}