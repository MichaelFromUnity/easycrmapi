﻿using EasyCRMApi.Data;
using EasyCRMApi.ModelProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyCRMApi.Hubs
{
    public class CourseHub : AbstractEntityHub<CourseProxy>
    {
        public override void CreateEntity(CourseProxy entity)
        {
            try
            {
                var course = new Course
                {
                    Name = entity.Name
                };
                var d = new EasyCRMDataContainer();
                var courseSet = d.CourseSet;
                courseSet.Add(course);
                d.SaveChanges();
                Clients.All.coursesUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void DeleteEntity(CourseProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var courseSet = d.CourseSet;
                var course = new Course { Id = entity.Id };
                courseSet.Attach(course);
                d.Entry(course).State = System.Data.Entity.EntityState.Deleted;
                d.SaveChanges();
                Clients.All.coursesUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }

        public override void GetAll()
        {
            Clients.Client(Context.ConnectionId).coursesUpdated(toProxyList());
        }

        public override void UpdateEntity(CourseProxy entity)
        {
            try
            {
                var d = new EasyCRMDataContainer();
                var courseSet = d.CourseSet;
                var client = new Course { Id = entity.Id, Name = entity.Name };
                courseSet.Attach(client);
                d.Entry(client).State = System.Data.Entity.EntityState.Modified;
                d.SaveChanges();
                Clients.All.coursesUpdated(toProxyList());
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
        }
        private List<CourseProxy> toProxyList()
        {
            var proxyList = new List<CourseProxy>();
            try
            {
                var d = new EasyCRMDataContainer();
                var courseSet = d.CourseSet;
                courseSet.ToList<Course>().ForEach(c => proxyList.Add(new CourseProxy { Id = c.Id, Name = c.Name }));
            }
            catch (Exception ex)
            {
                logger.Error(ex.ToString());
            }
            return proxyList;
        }
    }
}