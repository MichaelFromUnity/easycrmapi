﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyCRMApi.ModelProxy
{
    public class EventProxy : AbstractModelProxy
    {
        public int ClientId { get; set; }
        public int CourseId { get; set; }
    }
}