﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EasyCRMApi.ModelProxy
{
    public class CourseProxy : AbstractModelProxy
    {
        public string Name { get; set; }
    }
}